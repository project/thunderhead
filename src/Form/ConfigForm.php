<?php

namespace Drupal\thunderhead\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Factory\FactoryInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\thunderhead\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Excluded Condition.
   *
   * @var \Drupal\system\Plugin\Condition\RequestPath
   */
  protected $excluded;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Plugin\Factory\FactoryInterface $plugin_factory
   *   Factory for condition plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FactoryInterface $plugin_factory) {
    parent::__construct($config_factory);
    $this->excluded = $plugin_factory->createInstance('request_path');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'thunderhead.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'thunderhead_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('thunderhead.config');

    $form['thunderhead_site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('thunderhead_site_key'),
    ];

    $form['excludes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Exclusions'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    // Pages visibility plugin.
    $this->excluded->setConfig('pages', $config->get('thunderhead_exclude_list'));
    $form['excludes'] += $this->excluded->buildConfigurationForm([], $form_state);

    unset($form['excludes']['negate']);

    $form['excludes']['pages']['#title'] = $this->t('URL exclude list');

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('thunderhead.config')
      ->set('thunderhead_site_key', $form_state->getValue('thunderhead_site_key'))
      ->set('thunderhead_exclude_list', $form_state->getValue('pages'))
      ->save();
  }

}
