(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.thunderhead = {
    attach: function (context, settings) {
      var onetag = document.createElement('script');
      onetag.src = 'https://eu2.thunderhead.com/one/rt/js/one-tag.js?siteKey=' + settings.thunderhead.siteKey;

      if ($('head script[src="' + onetag.src + '"]').length > 0) {
        return;
      }

      onetag.id = 'thxTag';
      onetag.type = 'text/javascript';
      onetag.async = true;

      var firstScriptElement = document.getElementsByTagName('script')[0];
      firstScriptElement.parentNode.insertBefore(onetag, firstScriptElement);
    }
  };

})(jQuery, Drupal);
